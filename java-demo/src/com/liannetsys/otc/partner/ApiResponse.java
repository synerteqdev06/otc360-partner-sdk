package com.liannetsys.otc.partner;

public abstract class ApiResponse {
	private String sign;
	private int status;

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
