package com.liannetsys.otc.partner;

public class ActiveOrderList extends ApiResponse{
	private Order[] data;

	public Order[] getData() {
		return data;
	}

	public void setData(Order[] data) {
		this.data = data;
	}
}
