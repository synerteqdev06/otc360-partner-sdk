package com.liannetsys.otc.partner;

public class Balance {

	private float balance;
	private float frozen;
	public float getBalance() {
		return balance;
	}
	public void setBalance(float balance) {
		this.balance = balance;
	}
	public float getFrozen() {
		return frozen;
	}
	public void setFrozen(float frozen) {
		this.frozen = frozen;
	}
}
