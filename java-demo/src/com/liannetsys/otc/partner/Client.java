package com.liannetsys.otc.partner;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.digests.MD5Digest;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Client {

	private HttpClient http;
	private Properties config;
	private PrivateKey privateKey;
	private PublicKey publicKey;
	public static final int HTTP_SUCCESS = 200;
	public static final int HTTP_CREATED = 201;
	private static final ObjectMapper mapper = new ObjectMapper();

	public Client() throws IOException {
		this.config = new Properties();
		String configFile = System.getProperty("client.properties");
		if (StringUtils.isEmpty(configFile)) {
			try (InputStream is = ClassLoader.getSystemResourceAsStream("client.properties")) {
				if (is != null) {
					config.load(is);
				} else {
					throw new FileNotFoundException("client.properties");
				}
			}
		} else {
			try (FileInputStream fis = new FileInputStream(configFile)) {
				config.load(fis);
			}
		}
		this.http = HttpClientBuilder.create().build();
	}

	public String getUrl(String command) {
		return this.config.getProperty("host") + "/api/" + command;
	}

	public void includeSignature(List<NameValuePair> params)
			throws InvalidKeyException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException, IOException {
		params.add(new BasicNameValuePair("apiKey", this.config.getProperty("apiKey")));
		params.add(new BasicNameValuePair("ts", String.valueOf(System.currentTimeMillis())));

		List<NameValuePair> paramcopy = new ArrayList<>(params);
		paramcopy.sort(new Comparator<NameValuePair>() {
			@Override
			public int compare(NameValuePair o1, NameValuePair o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		StringBuilder sb = new StringBuilder();
		for (NameValuePair nvp : paramcopy) {
			sb.append(nvp.getValue());
		}
		String signature = generateSignature(sb);
		params.add(new BasicNameValuePair("sign", signature));
	}

	private static PrivateKey createPrivateKeyFromPemFile(String keyFileName)
			throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
		// Loads a private key from the specified key file name
		try (FileReader fr = new FileReader(keyFileName); PEMParser parser = new PEMParser(fr)) {
			Object obj = parser.readObject();
			JcaPEMKeyConverter converter = new JcaPEMKeyConverter();
			if (obj instanceof PrivateKeyInfo) {
				return converter.getPrivateKey((PrivateKeyInfo) obj);
			} else if (obj instanceof PEMKeyPair) {
				PrivateKeyInfo privateKeyInfo = ((PEMKeyPair) obj).getPrivateKeyInfo();
				if (privateKeyInfo != null) {
					return converter.getPrivateKey(privateKeyInfo);
				}
			}
		}
		throw new InvalidKeySpecException("Invalid key.");
	}

	private static PublicKey loadPublicKeyFromPemFile(String keyFileName)
			throws FileNotFoundException, IOException, InvalidKeySpecException {
		// Loads a public key from the specified key file name
		try (FileReader fr = new FileReader(keyFileName); PEMParser parser = new PEMParser(fr)) {
			Object obj = parser.readObject();
			JcaPEMKeyConverter converter = new JcaPEMKeyConverter();
			if (obj instanceof SubjectPublicKeyInfo) {
				return converter.getPublicKey((SubjectPublicKeyInfo) obj);
			} else if (obj instanceof PEMKeyPair) {
				SubjectPublicKeyInfo publicKeyInfo = ((PEMKeyPair) obj).getPublicKeyInfo();
				if (publicKeyInfo != null) {
					return converter.getPublicKey(publicKeyInfo);
				}
			}
		}
		throw new InvalidKeySpecException("Invalid key .");
	}

	public boolean validateSignature(String data, String signature)
			throws FileNotFoundException, InvalidKeySpecException, IOException, NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidKeyException {
		MD5Digest md5 = new MD5Digest();
		byte[] bytes=data.getBytes("UTF-8");
		md5.update(bytes, 0, bytes.length);
		// get the output/ digest size and hash it
		byte[] digest = new byte[md5.getDigestSize()];
		md5.doFinal(digest, 0);
		String hash = Hex.encodeHexString(digest);
		if (publicKey == null) {
			publicKey = loadPublicKeyFromPemFile(this.config.getProperty("public.keyfile"));
		}
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.DECRYPT_MODE, publicKey);
		byte[] rawSignature = Base64.decodeBase64(signature);
		try {
			byte[] decrypted = cipher.doFinal(rawSignature);
			String sign = new String(decrypted, "UTF-8");
			return hash.equals(sign);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String generateSignature(StringBuilder sb) throws InvalidKeySpecException, NoSuchAlgorithmException,
			IOException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		MD5Digest md5 = new MD5Digest();
		byte[] bytes=sb.toString().getBytes("UTF-8");
		md5.update(bytes, 0, bytes.length);
		// get the output/ digest size and hash it
		byte[] digest = new byte[md5.getDigestSize()];
		md5.doFinal(digest, 0);
		String hash = Hex.encodeHexString(digest);
		if (privateKey == null) {
			privateKey = createPrivateKeyFromPemFile(this.config.getProperty("private.keyfile"));
		}
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.ENCRYPT_MODE, privateKey);
		return Base64.encodeBase64String(cipher.doFinal(hash.getBytes()));
	}

	private <T> T sendPostCommand(String url, List<NameValuePair> params, int successCode, Class<T> targetClass)
			throws ClientProtocolException, IOException, InvalidKeyException, InvalidKeySpecException,
			NoSuchAlgorithmException, NoSuchPaddingException {
		HttpPost post = new HttpPost(url);
		post.setEntity(new UrlEncodedFormEntity(params));
		HttpResponse response = http.execute(post);
		return processResponse(successCode, targetClass, response);
	}

	private <T> T processResponse(int successCode, Class<T> targetClass, HttpResponse response)
			throws IOException, InvalidKeyException, FileNotFoundException, InvalidKeySpecException,
			NoSuchAlgorithmException, NoSuchPaddingException, JsonProcessingException {
		int statusCode = response.getStatusLine().getStatusCode();
		try (InputStream is = response.getEntity().getContent()) {
			String responseText = IOUtils.toString(is, Charset.forName("UTF-8"));
			System.out.println(responseText);
			if (successCode == statusCode) {
				JsonNode node = mapper.readTree(responseText);
				int status = node.get("status").asInt();
				if (status == 1) {
					if (node.has("sign")) {
						if (validateSignature(node)) {
							return mapper.treeToValue(node, targetClass);
						} else {
							throw new RuntimeException("Invalid Response Signature");
						}
					}
					return mapper.treeToValue(node, targetClass);
				}
			}
			throw new RuntimeException("API call failed " + responseText);
		}
	}

	private boolean validateSignature(JsonNode node) throws InvalidKeyException, FileNotFoundException,
			InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException, IOException {
		StringBuilder sb = new StringBuilder();
		generateKeyString(node, sb, false);
		return validateSignature(sb.toString(), node.get("sign").asText());
	}
	
	private void generateKeyString(JsonNode node, StringBuilder sb, boolean isDataObject) {
		if (node.isValueNode()) {
			sb.append(node.asText());			
		} else {
			List<String> keys = new ArrayList<>();
			if(node.isArray()) {
				for(JsonNode item : node) {
					generateKeyString(item, sb, true);
				}
			} else { 
				for (Iterator<String> it = node.fieldNames(); it.hasNext();) {
					keys.add(it.next());
				}
				keys.sort(new Comparator<String>() {
					@Override
					public int compare(String o1, String o2) {
						return o1.compareTo(o2);
					}
				});
				for (String key : keys) {
					JsonNode field = node.get(key);
					if (isDataObject || (!"sign".equals(key) && !"status".equals(key))) {
						generateKeyString(field, sb, true);
					}
				}				
			}
		}
	}
	
	public PartnerBalance getPartnerBalance(String phoneNum)
			throws InvalidKeyException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException, IOException {
		String url = getUrl("partner_balance");
		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("phoneNum", phoneNum));
		includeSignature(params);
		return sendPostCommand(url, params, HTTP_CREATED, PartnerBalance.class);
	}
	
	public ActiveOrderList getActiveOrderList(String phoneNum)
			throws InvalidKeyException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException, IOException {
		String url = getUrl("partner_active");
		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("phoneNum", phoneNum));
		includeSignature(params);
		return sendPostCommand(url, params, HTTP_CREATED, ActiveOrderList.class);
	}

	public OrderInfo getOrderInfo(String refNum)
			throws InvalidKeyException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException, IOException {
		String url = getUrl("get_order");
		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("referenceNo", refNum));
		includeSignature(params);
		return sendPostCommand(url, params, HTTP_CREATED, OrderInfo.class);
	}

	public static void main(String args[]) throws Exception {		
		Client client = new Client();
		String phoneNum = "";
		PartnerBalance bal = client.getPartnerBalance(phoneNum);
		System.out.println("available balance: " + bal.getData().getBalance());
		
		ActiveOrderList active = client.getActiveOrderList(phoneNum);
		System.out.println("number of active orders: " + active.getData().length);
		
		String referenceNum = "";
		OrderInfo order = client.getOrderInfo(referenceNum);
		System.out.println("order ref: " + order.getData().getReference() + "; status: " + order.getData().getStatus());
	}
}